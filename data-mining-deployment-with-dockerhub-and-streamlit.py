import os
import requests
import pandas as pd
import streamlit as st
import matplotlib.pyplot as plt

from bs4 import BeautifulSoup

### URL CONTENT PULLING
# Getting the web page to be scrapped into the code
url_path = "https://www.value.today/world/world-top-500-companies"
responses = requests.get(url_path)

### STATUS CODE CHECK
# It is important to check the legibility of the web-page to scrape to see if scraping is possible on it.
# It should be noted that this can be done using robots.txt before pulling the web content, OR;
# After the web content has been pulled by checking the status_code.
# Status code response in the range (200 - 299) means the web content can be scraped otherwise not.
print(responses.status_code)

### INSPECTION AND EXTRACTION
# This is the stage where we inspect the web content we just pulled, AND;
# Extraction of the useful data / information we wish to document for further research and analysis.
soup = BeautifulSoup(responses.text, 'html.parser')
companies = soup.find_all('div', {'class': 'row well views-row'})

### Data Extraction Operation
world_rank = []
for tag in companies:
    rank = tag.find('div', {'class': 'views-field views-field-field-world-rank-jan-2020 clearfix col-sm-12'})
    try:
        world_rank.append(int(rank.find('span').text))
    except AttributeError:
        world_rank.append(None)

market_capital = []
for tag in companies:
    capital_market = tag.find('div',
                              {'class': 'views-field views-field-field-market-value-jan-2020 clearfix col-sm-12'})
    try:
        market_capital_value = capital_market.find('span', {'class': 'field-content'})
        market_capital.append(market_capital_value.text)
    except AttributeError:
        market_capital.append(None)

head_quarters = []
for tag in companies:
    head_quarter = tag.find('div',
                            {'class': 'views-field views-field-field-headquarters-of-company clearfix col-sm-12'})
    try:
        head_quarters.append(head_quarter.find('span').text)
    except AttributeError:
        head_quarters.append(None)

sectors = []
for tag in companies:
    sector_tag = tag.find('div', {'class': 'views-field views-field-field-company-category-primary clearfix col-sm-12'})
    try:
        sectors.append(sector_tag.find('span').text)
    except AttributeError:
        sectors.append(None)

annual_revenue = []
for tag in companies:
    revenue = tag.find('div', {'class': 'views-field views-field-field-annual-revenue clearfix col-sm-12'})
    try:
        annual_revenue.append(revenue.find('span').text)
    except AttributeError:
        annual_revenue.append(None)

annual_net_income = []
for tag in companies:
    net_income = tag.find('div', {'class': 'views-field views-field-field-annual-net-income-lc clearfix col-sm-12'})
    try:
        annual_net_income.append(net_income.find('span').text)
    except AttributeError:
        annual_net_income.append(None)

total_assets = []
for tag in companies:
    assets = tag.find('div', {'class': 'views-field views-field-field-total-assets clearfix col-sm-12'})
    try:
        total_assets.append(assets.find('span').text)
    except AttributeError:
        total_assets.append(None)

total_employees = []
for tag in companies:
    employees = tag.find('div', {'class': 'views-field views-field-field-employee-count clearfix col-sm-12'})
    try:
        total_employees.append(employees.find('span').text)
    except AttributeError:
        total_employees.append(None)

ceos = []
for tag in companies:
    ceo_ = tag.find('div', {'class': 'views-field views-field-field-ceo clearfix col-sm-12'})
    try:
        ceo = ceo_.find('span', {'class': 'field-content'})
        ceos.append(ceo.text)
    except AttributeError:
        ceos.append(None)

### CREATING DICTIONARY OF THE EXTRACTED DATA
companies = {
    "World Rank": world_rank,
    "Market Capital": market_capital,
    "Headquarters": head_quarters,
    "Sectors": sectors,
    "Annual Revenue": annual_revenue,
    "Annual Net Income": annual_net_income,
    "Total Assests": total_assets,
    "Total Employees": total_employees,
    "CEOs": ceos
}

### CONVERTING DICTIONARY INTO DATAFRAME
company_data_frame = pd.DataFrame(companies, dtype=object)

### SAVING THE DATAFRAME AS A CSV FILE
file_name = "/Top 500 Companies.csv"
store_path = os.getcwd() + file_name
company_data_frame.to_csv(store_path)

### Reading the save .csv file back as Pandas DataFrame
company_df = pd.read_csv(store_path, index_col=False)
company_df = company_df.drop("Unnamed: 0", axis=1)

### Initiating Streamlit
st.header('Data Mining Deployment')
st.markdown('# DEPLOYMENT OF THE DATA MINED FROM TOP 500 COMPANIES')
st.write(
    """
        This is a Project based on mining data from a website using the library BeautifulSoup,\n
        containerizing the extracted data and deploying the extracted Data using Streamlit.\n
        It contains 9 columns and 500 records.\n
    """
)

### Displaying the Extracted Dataset
st.dataframe(company_df)




