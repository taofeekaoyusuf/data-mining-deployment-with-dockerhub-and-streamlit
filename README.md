# DATA MINING WITH BEAUTIFULSOUP


### Brief Info about the Project

![Data Mining at its best](https://gitlab.com/taofeekaoyusuf/data-mining-deployment-with-dockerhub-and-streamlit/-/raw/main/ws-n-dm.jpeg)

- This project showcase how the power of `urllib` and `beautifulsoup` libraries can be earness to mine data on the web.

- It is a project that can be further fine-tuned and automated to be leverage for other website data mining as well.


## Conteinerizing the App
`docker build -t mined-data .` 

## App image deployed to dockerhub
`docker push dhackbility/mined-data:v1.0` 

- In order to run the Containerized app, it must be pull from the Dockerhub respository, respectively thus:
## Getting the Containerized App from Dockerhub respository
`docker pull dhackbility/mined-data:v1.0` 

## Runnig the Containerized App
`docker run -dp 8501:8501 dhackbility/mined-data:v1.0` 

- The mined data app deployed using streamlit could be viewed by going to the following local address:
`http://localhost:8501`

- Clicking on the Breadcrumb icon at the upper-right corner and clicking on settings,the `RUN ON SAVE` and the `WIDE MODE` settings can be checked.

- Of course, further work can still be done on the Project. 

- Thank you for reading thus far, enjoy playing around with the Project.




