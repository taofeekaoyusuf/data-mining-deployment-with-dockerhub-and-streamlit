FROM python:3.10.4-slim

RUN mkdir /withBeautifulSoup

WORKDIR /withBeautifulSoup

COPY . /withBeautifulSoup

RUN pip install -r requirements.txt

EXPOSE 8501

ENTRYPOINT ["streamlit", "run"]

CMD ["data-mining-deployment-with-dockerhub-and-streamlit.py"]
